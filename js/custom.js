// if ($(window).width() > 991) {
// $(window).scroll(function(e){ 
//   var $el = $('.bet-parent'); 
//   var isPositionFixed = ($el.css('position') == 'fixed');
//   if ($(this).scrollTop() > 160 && !isPositionFixed){ 
//     $el.css({'position': 'fixed', 'top': '0px', 'right': '10px'});
//     $('.bet-parent') .css({'height': (($(window).height()) - 0)+'px'}); 
//   }

//   if ($(this).scrollTop() < 160 && isPositionFixed){
//     $el.css({'position': 'static', 'top': '0px', 'right': '10px'}); 
//     $('.bet-parent') .css({'height': (($(window).height()) - 170)+'px'});
//   } 
// });
// }


$(document).ready(function() {
    $("#accordian a").click(function() {
        var link = $(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;

        closest_ul.find("ul").slideUp(function() {
            if (++count == closest_ul.find("ul").length)
                parallel_active_links.removeClass("active");
        });

        if (!link_status) {
            closest_li.children("ul").slideDown('50000');
            closest_li.addClass("active");
        }
    })
})

$(document).ready(function() {
  $('.table-open').click(function() {
    $('tbody').slideToggle("fast");
  });
});

$(document).ready(function() {
  var owl = $('.owl-carousel');
  owl.owlCarousel({
    items: 6,
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true
  });
});


$(document).ready(function() {
    $('.minus').click(function () {
      var $input = $(this).parent().find('input');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
    });
    $('.plus').click(function () {
      var $input = $(this).parent().find('input');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    });
  });

//   $(function() {
//     if ($(window).width() > 991) {
//         $('.bet-parent') .css({'height': (($(window).height()) - 160)+'px'});
    
//         $(window).bind('resize', function(){
//             $('.bet-parent') .css({'height': (($(window).height()) - 160)+'px'});
//         });
//     }
// });


  $(document).ready(function() {
    if ($(window).width() > 991) {
    var $scrollable_div1 = $('.bet-parent');
    if ($scrollable_div1.data('scrollator') === undefined) {
        $scrollable_div1.scrollator();}
    }
});

function toggleIcon(e) {
        $(e.target)
            .prev('.mkt-head')
            .find(".svg-inline--fa")
            .toggleClass('fa-angle-right fa-angle-up');
    }
    $('.mkt-odds').on('hidden.bs.collapse', toggleIcon);
    $('.mkt-odds').on('shown.bs.collapse', toggleIcon);


function openNav() {
  document.getElementById("navbarSupportedContent").style.right = "0px";
  $('.close-menu').show();
  $("body").append("<div class='menu-modal-backdrop'>");
}

function closeNav() {
  document.getElementById("navbarSupportedContent").style.right= "-250px";
  $('.close-menu').hide();
  $('.menu-modal-backdrop').remove();
}

function Tabs() {
  var bindAll = function() {
    var menuElements = document.querySelectorAll('[data-tab]');
    for(var i = 0; i < menuElements.length ; i++) {
      menuElements[i].addEventListener('click', change, false);
    }
  }

  var clear = function() {
    var menuElements = document.querySelectorAll('[data-tab]');
    for(var i = 0; i < menuElements.length ; i++) {
      menuElements[i].classList.remove('active');
      var id = menuElements[i].getAttribute('data-tab');
      document.getElementById(id).classList.remove('active');
    }
  }

  var change = function(e) {
    clear();
    e.target.classList.add('active');
    var id = e.currentTarget.getAttribute('data-tab');
    document.getElementById(id).classList.add('active');
  }

  bindAll();
}

var connectTabs = new Tabs();


$(window).scroll(function() {
  var height = $(window).scrollTop();
  if (height > 100) {
    $('#back-top').fadeIn();
  } else {
    $('#back-top').fadeOut();
  }
});
$(document).ready(function() {
  $("#back-top").click(function(event) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });
});